<?php
function ubah_huruf($string)
{
    $kamus = "abcdefghijklmnopqrstuvwxyz";
    $hasil = "";
    for ($i = 0; $i < strlen($string); $i++) {
        if ($string[$i] == "z") {
            $hasil .= "a";
        } else {
            for ($j = 0; $j < strlen($kamus); $j++) {
                if ($string[$i] == $kamus[$j]) {
                    $hasil .= $kamus[$j + 1];
                }
            }
        }
    }
    return $hasil . "\n";
}

// TEST CASES
echo ubah_huruf('wow'); // xpx
echo ubah_huruf('developer'); // efwfmpqfs
echo ubah_huruf('laravel'); // mbsbwfm
echo ubah_huruf('keren'); // lfsfo
echo ubah_huruf('semangat'); // tfnbohbu
